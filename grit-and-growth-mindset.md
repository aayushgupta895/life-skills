
# Grit and Growth Mindset

**Answer 1**

* Grit combines passion and perseverance towards long-term goals.
* It reflects commitment and diligence over an extended period.
* Individuals with high grit are more likely to achieve their aspirations.
* Grit is akin to stamina in its measure of commitment to future objectives.
* Those with high grit often contribute effectively to groups or organizations they deeply commit to.

**Answer 2**

* Growth mindset: Belief that nobody is inherently extraordinary; everyone can learn and develop skills to reach their goals.
* Focus on process: Emphasize improving performance rather than just outcomes.
* Embrace challenges: See challenges as opportunities for growth.
* Extra effort: Willing to put in additional work to progress.
* Learn from mistakes: View mistakes as learning opportunities.
* Positive view of feedback: See feedback as a constructive tool for improvement.

**Answer 3**

* **Internal locus of control** : Belief in personal control over life and outcomes.
* **External locus of control**: Belief that external factors dictate life.
* Cultivating internal locus: Solve problems, recognize personal actions as solutions.
* Characteristics: Motivation, persistence, resilience.
* Resilience: Ability to bounce back from setbacks.

**Answer 4**

**The speaker focuses on essential elements for nurturing a growth mindset:**
* Question your beliefs.
* Believe in your problem-solving skills.
* Challenge your assumptions.
* Take charge of your life.
* Value overcoming challenges.

**Answer 5**

**These will be my strategies for developing a growth mindset:**
* Value effort and progress over outcomes.
* Embrace challenges for growth.
* Trust in hard work for success.
* Seek and accept feedback.
* Reflect on mistakes for improvement.
* Use positive self-talk for motivation.
