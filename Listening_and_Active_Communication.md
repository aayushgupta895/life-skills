# Answer 1
* Focus only on the speaker by turning off gadgets and avoiding doing multiple things.

* Show you're paying attention with nods, eye contact, and open body language.

* Repeat what you heard in your own words to be sure you get it.

* Ask questions to learn more and clear up any confusion.

* Wait for the speaker to finish talking before you say anything, and don't cut in.

* Give feedback like saying "I get it" to show you understand.

# Answer 2
* Repeat what the speaker said in your own words to show you got it. This helps make sure you understood correctly.

* Show you understand and care about how the speaker feels. Reflective listening means recognizing and respecting their emotions, so they feel heard and supported.

* Use different words to say the same thing. Paraphrasing helps clarify the speaker's ideas and shows you're paying attention.

* Regularly check with the speaker to make sure you're understanding them right. Summarize or repeat important points to confirm you're on the same page and avoid confusion.

# Answer 3
If I'm not interested or think I already know what someone's saying, I might stop listening and miss important details.

# Answer 4
To show I'm engaged, I should give my full attention, wait for them to finish, and ask questions to show I understand and care. 

# Answer 5
I tend to stay quiet if I don't think the topic is important, as I doubt my opinions will make a difference.

# Anweer 6
Normally, I prefer to be respectful and open when I talk, but I'll speak up if it's important or to set clear rules for others' well-being.

# Answer 7
When I'm frustrated or feel misunderstood, I might use sarcasm or hold back details instead of expressing my concerns directly.

# Answer 8
Communicate clearly, understand others' perspectives, and be open to finding common ground even if we disagree.