## **Object-Oriented Programming Principles**

#### Abstract:
 Object-Oriented Programming (OOP) is a crucial paradigm in software development, offering a structured approach to code organization, maintenance, and scalability. This paper explores key OOP principles and their application in refactoring a complex JavaScript codebase. We delve into concepts such as encapsulation, inheritance, polymorphism, and abstraction, illustrating how they can enhance code quality and structure. Through JavaScript code samples and examples, we showcase common code smells and refactorings aligned with OOP principles, aiding developers in transforming unwieldy codebases into more maintainable and efficient systems.

#### Introduction:
Object-Oriented Programming (OOP) encourages the creation of reusable and modular components, known as objects, facilitating the development of scalable and maintainable software systems. In this paper, we will explore the fundamental principles of OOP and their significance in refactoring a complex JavaScript codebase. Through practical examples and code snippets, we will demonstrate how adherence to these principles can lead to improved code organization, readability, and maintainability.

#### Encapsulation:
Encapsulation involves bundling data and methods that operate on the data within a single unit, typically a class in JavaScript. By encapsulating related functionality, we can control access to the internal state of an object, promoting information hiding and reducing dependencies. Let's consider a simpler example unrelated to cars, such as a basic calculator. Here's how we can encapsulate the functionality of addition and subtraction within a `Calculator` class:

```
class Calculator {
  constructor() {
    this.result = 0; // Initial result
  }

  add(num) {
    // Add a number to the result
    this.result += num;
  }

  subtract(num) {
    // Subtract a number from the result
    this.result -= num;
  }

  clear() {
    // Reset the result to zero
    this.result = 0;
  }
}

// Usage:
const calc = new Calculator();
console.log("Initial result:", calc.result); // Output: 0

calc.add(5);
console.log("After adding 5:", calc.result); // Output: 5

calc.subtract(3);
console.log("After subtracting 3:", calc.result); // Output: 2

calc.clear();
console.log("After clearing:", calc.result); // Output: 0

```
In this example:

-   We have a `Calculator` class with methods `add()`, `subtract()`, and `clear()` for performing addition, subtraction, and resetting the result, respectively.
-   The `result` attribute holds the current calculated result, and it's directly accessible.
-   Each method encapsulates the functionality of performing the respective operation while controlling access to the result.
 
#### Inheritance:~ 
Inheritance allows a class to inherit properties and behavior from another class, promoting code reuse and hierarchical relationships. Consider the following example:
```
class Animal {
  speak() {
    // Abstract method
  }
}

class Dog extends Animal {
  speak() {
    return "Woof!";
  }
}

class Cat extends Animal {
  speak() {
    return "Meow!";
  }
}` 
```
In this example, both `Dog` and `Cat` inherit from the `Animal` class and override the `speak()` method to provide their own implementation.

#### Polymorphism:
 Polymorphism enables objects of different classes to be treated as objects of a common superclass, promoting flexibility and extensibility. Consider the following example:

```
function makeSound(animal) {
  return animal.speak();
}

const dog = new Dog();
const cat = new Cat();

console.log(makeSound(dog)); // Output: Woof!
console.log(makeSound(cat)); // Output: Meow!` 
```
#### Abstraction: 
Abstraction involves hiding complex implementation details behind a simplified interface. It allows developers to focus on essential features while hiding unnecessary complexity. Consider the following example:


```
class Shape {
  area() {
    // Abstract method
  }
}

class Rectangle extends Shape {
  constructor(width, height) {
    super();
    this.width = width;
    this.height = height;
  }

  area() {
    return this.width * this.height;
  }
}` 
```
In this example, the `Shape` class defines an abstract method `area()`, which must be implemented by concrete subclasses such as `Rectangle`.

#### Conclusion:
 Object-Oriented Programming principles provide a robust framework for structuring JavaScript codebases, leading to improved maintainability, scalability, and flexibility. By embracing concepts such as encapsulation, inheritance, polymorphism, and abstraction, developers can refactor complex codebases into more organized and manageable systems. Through careful application of OOP principles, JavaScript projects can achieve enhanced code quality, readability, and maintainability, ultimately contributing to a more efficient development process.

#### References:
- MDN Web Docs (https://developer.mozilla.org/en-US/docs/Web/JavaScript) - The Mozilla Developer Network provides comprehensive documentation on JavaScript, including OOP concepts and usage. 
- freeCodeCamp (https://www.freecodecamp.org/news/object-oriented-javascript-for-beginners/) - their website and youtube video tutorial covers everything about oops.

Author Name:
Aayush Gupta  
aayush.gupta.27.2@mountblue.tech