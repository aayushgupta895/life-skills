# Energy Management

## 1. Manage Energy not Time

#### Question 1: What are the activities you do that make you relax - Calm quadrant?

- Music
- Video Games

#### Question 2: When do you find getting into the Stress quadrant?

- I tell myself I can't solve a problem even before I try.
- I delay doing things, then rush to finish them just before they're due.

#### Question 3: How do you understand if you are in the Excitement quadrant?

- When the dopamine hits
- Achievements which lead to satisfaction

## 4. Sleep is your superpower

#### Question 4: Paraphrase the Sleep is your Superpower video in detail.

- Lack of sleep affects both mental and physical health.
- Sleep acts like a "save" button for our memories, helping turn daily experiences into permanent memories and preparing the brain for learning.
- Without enough sleep, there's a 40% drop in the brain's ability to form new memories.
- You can't "catch up" on missed sleep or accumulate a sleep debt to pay off later.
- Not getting enough sleep is harmful, and humans are unique in deliberately depriving themselves of sleep.
- Sleep is incredibly valuable for overall health and well-being, often likened to an elixir of immortality.

#### Question 5: What are some ideas that you can implement to sleep better?

- Sleep and wake up at the same time every day.
- Don't work in the room where you sleep.

## 5. Brain Changing Benefits of Exercise

#### Question 6: Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points.

- Exercise helps us focus better and improves long-term memory.
- It's incredibly transformative for the brain:
  - Immediate effects include increased levels of dopamine and serotonin.
- Exercise also has long-lasting effects:
  - It creates new cells in the hippocampus, improving long-term memory.
- Our mood improves with exercise.
- It protects the brain by enlarging the prefrontal cortex and hippocampus, reducing the risk of dementia and Alzheimer's disease.
- Aim for three to four exercise sessions per week, lasting at least 30 minutes each.

#### Question 7: What are some steps you can take to exercise more?

- Take stairs to the office
- Take walks daily
